from fractions import Fraction
from typing import Any

from page_layout.params import Params


# the to_dict method has been removed, but keeping the tests
def to_dict(params: Params) -> dict[str, Any]:
    return {
        "full_page": "1" if params.full_page else "0",
        "caption_position": params.caption_position or "",
        "caption_columns": params.caption_columns,
        "caption_font_size": ("" if params.caption_font_size is None else str(params.caption_font_size)),
        "shift_page_number": "1" if params.shift_page_number else "0",
        "gutter": "" if params.gutter is None else str(params.gutter),
        "page_aspect": params.aspect,
        "page_mode": params.page_mode or "",
        "page_colour": params.page_colour or "",
    }


def test_key() -> None:
    assert Params().key() == ("default", 0, "6x8")


def test_layout_params() -> None:
    assert Params().layout_params(Fraction(123)).key() == (
        Fraction(123),
        "default",
        Fraction(0),
        "6x8",
    )


def test_options_none() -> None:
    p = Params()
    assert p.options(None) is p


def test_options() -> None:
    defaults = Params(page_colour="black")
    assert to_dict(Params(page_colour=None).options(defaults)) == {
        "full_page": "0",
        "caption_position": "off",
        "caption_columns": 1,
        "caption_font_size": "8",
        "shift_page_number": "1",
        "gutter": "0",
        "page_aspect": "6x8",
        "page_mode": "default",
        "page_colour": "black",
    }


def test_to_dict_defaults() -> None:
    assert to_dict(Params()) == {
        "full_page": "0",
        "caption_position": "off",
        "caption_columns": 1,
        "caption_font_size": "8",
        "shift_page_number": "1",
        "gutter": "0",
        "page_aspect": "6x8",
        "page_mode": "default",
        "page_colour": "white",
    }


def test_to_dict() -> None:
    assert to_dict(
        Params(
            page_mode=None,
            gutter=None,
            page_colour=None,
            full_page=True,
            caption_font_size=None,
            shift_page_number=False,
        )
    ) == {
        "full_page": "1",
        "caption_position": "off",
        "caption_columns": 1,
        "caption_font_size": "",
        "shift_page_number": "0",
        "gutter": "",
        "page_aspect": "6x8",
        "page_mode": "",
        "page_colour": "",
    }


def test_for_cover_front() -> None:
    p = Params.for_cover("front")
    assert p.scale == 1.0
    assert to_dict(p) == {
        "full_page": "1",
        "caption_position": "bottom",
        "caption_columns": 1,
        "caption_font_size": "8",
        "shift_page_number": "1",
        "gutter": "",
        "page_aspect": None,
        "page_mode": "",
        "page_colour": "",
    }


def test_for_cover_back() -> None:
    p = Params.for_cover("back")
    assert p.scale == 0.75
    assert to_dict(p) == {
        "full_page": "0",
        "caption_position": "bottom",
        "caption_columns": 1,
        "caption_font_size": "8",
        "shift_page_number": "1",
        "gutter": "",
        "page_aspect": None,
        "page_mode": "",
        "page_colour": "",
    }
