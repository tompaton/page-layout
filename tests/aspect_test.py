from fractions import Fraction

import pytest

from page_layout import aspect


def test_get_aspect2() -> None:
    x = aspect.get_aspect("6x8")
    assert x in aspect.ASPECTS
    assert x.code == "6x8"


def test_get_aspect2_bad() -> None:
    with pytest.raises(ValueError, match="1x2"):
        aspect.get_aspect("1x2")


@pytest.mark.parametrize(
    ("width", "height", "valid"),
    [(3, 4, True), (6, 8, True), (4, 3, True), (4, 4, True), (1, 9, False)],
)
def test_valid(width: float, height: float, valid: bool) -> None:
    x = aspect.get_aspect("6x8")
    assert x.valid(Fraction(width), Fraction(height)) == valid


@pytest.mark.parametrize(
    ("crop", "orientation", "width", "height"),
    [
        ("inside", "landscape", 800, 600),
        ("inside", "portrait", 450, 600),
        ("inside", "square", 600, 600),
        ("outside", "landscape", 800, 600),
        ("outside", "portrait", 800, Fraction(3200, 3)),
        ("outside", "square", 800, 800),
    ],
)
def test_crop1(crop: str, orientation: str, width: float, height: float) -> None:
    x = aspect.get_aspect("6x8")
    assert x.crop(800, 600, crop, orientation) == (
        Fraction(width),
        Fraction(height),
    )


@pytest.mark.parametrize(
    ("crop", "orientation", "width", "height"),
    [
        ("inside", "landscape", 600, 450),
        ("inside", "portrait", 600, 800),
        ("inside", "square", 600, 600),
        ("outside", "landscape", Fraction(3200, 3), 800),
        ("outside", "portrait", 600, 800),
        ("outside", "square", 800, 800),
    ],
)
def test_crop2(crop: str, orientation: str, width: float, height: float) -> None:
    x = aspect.get_aspect("6x8")
    assert x.crop(600, 800, crop, orientation) == (
        Fraction(width),
        Fraction(height),
    )
