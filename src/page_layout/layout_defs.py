# ignore invalid argument/variable name
# ruff: noqa: N803, N806

# ignore unused variable/argument
# ruff: noqa: ARG001

# ignore too many variables/arguments
# pylint: disable=R0913,R0914

from collections import defaultdict

from page_layout.caption import Caption as C  # noqa
from page_layout.layout import GenSlots, Layout


# ignore unused local variables
# ruff: noqa: F841
def generate_layouts() -> list[Layout]:
    l1 = Layout("l1").generate(1, _generate_layouts_l1())
    p1 = l1.transpose("p1", [1])
    # copy of p1 layout that won't be scaled
    p1a = l1.transpose("p1a", [1])
    s1 = Layout("s1").generate(1, _generate_layouts_s1())

    caption2 = [C.Top, C.Bottom]
    l2 = Layout("l2").generate(2, _generate_layouts_l2(), caption2)
    p2 = l2.transpose("p2", [1, 2])
    l2.flip(l2, [2, 1])
    p2.flip(p2, [2, 1])

    # version of l3 with only two slots
    l2a = Layout("l2a").generate(2, _generate_layouts_l2a(), caption2)

    s2a = Layout("s2a").generate(2, _generate_layouts_s2a(), caption2)
    s2b = s2a.transpose("s2b", [1, 2])
    s2a.flip(s2a, [2, 1])
    s2b.flip(s2b, [2, 1])
    s2a.flip(s2b, [1, 2])

    def _pll(pll_id, lpp_id, llp_id, ppl_id, gen_slots) -> None:  # type: ignore
        pll = Layout(pll_id).generate(3, gen_slots, [C.Left, C.TopRight, C.BottomRight])
        lpp = pll.transpose(lpp_id, [1, 2, 3])
        llp = pll.flip_h(llp_id, [2, 3, 1])
        ppl = lpp.flip_v(ppl_id, [2, 3, 1])
        pll.flip(pll, [1, 3, 2])
        llp.flip(llp, [2, 1, 3])
        lpp.flip(lpp, [1, 3, 2])
        ppl.flip(ppl, [2, 1, 3])

    _pll("pll", "lpp", "llp", "ppl", _generate_layouts_pll())
    _pll("pss", "lss", "ssp", "ssl", _generate_layouts_pss())

    l3 = Layout("l3").generate(3, _generate_layouts_l3(), [C.Top, C.Middle, C.Bottom])
    p3 = l3.transpose("p3", [1, 2, 3])
    l3.flip(l3, [3, 2, 1])
    p3.flip(p3, [3, 2, 1])

    caption4 = [C.TopLeft, C.TopRight, C.BottomLeft, C.BottomRight]
    l4 = Layout("l4").generate(4, _generate_layouts_l4(), caption4)
    s4 = Layout("s4").generate(4, _generate_layouts_s4(), caption4)
    p4 = l4.transpose("p4", [1, 3, 2, 4])
    l4.flip(l4, [2, 1, 4, 3])
    l4.flip(l4, [3, 4, 1, 2])
    p4.flip(p4, [2, 1, 4, 3])
    p4.flip(p4, [3, 4, 1, 2])
    s4.flip(s4, [2, 1, 4, 3])
    s4.flip(s4, [3, 4, 1, 2])

    def _lplp(lplp_id, llpp_id, plpl_id, ppll_id, gen_slots) -> None:  # type: ignore
        lplp = Layout(lplp_id).generate(4, gen_slots, caption4)
        ppll = lplp.transpose(ppll_id, [1, 3, 2, 4])
        plpl = lplp.flip_h(plpl_id, [2, 1, 4, 3])
        llpp = ppll.flip_v(llpp_id, [3, 4, 1, 2])
        lplp.flip(lplp, [3, 4, 1, 2])
        plpl.flip(plpl, [3, 4, 1, 2])
        ppll.flip(ppll, [2, 1, 4, 3])
        llpp.flip(llpp, [2, 1, 4, 3])

    _lplp("lplp", "llpp", "plpl", "ppll", _generate_layouts_lplp())
    _lplp("lsls", "sspp", "slsl", "ppss", _generate_layouts_lsls())

    lppl = Layout("lppl").generate(4, _generate_layouts_lppl(), caption4)
    pllp = lppl.flip_h("pllp", [2, 1, 4, 3])
    lppl.flip(pllp, [3, 1, 4, 2])

    def _l3p2(l3p2_id, p3l2_id, p2l3_id, l2p3_id, gen_slots) -> None:  # type: ignore
        l3p2 = Layout(l3p2_id).generate(
            5,
            gen_slots,
            [C.TopLeft, C.MiddleLeft, C.BottomLeft, C.TopRight, C.BottomRight],
        )
        p3l2 = l3p2.transpose(p3l2_id, [1, 2, 3, 4, 5])
        p2l3 = l3p2.flip_h(p2l3_id, [4, 5, 1, 2, 3])
        l2p3 = p3l2.flip_v(l2p3_id, [4, 5, 1, 2, 3])
        l3p2.flip(l3p2, [3, 2, 1, 5, 4])
        p2l3.flip(p2l3, [2, 1, 5, 4, 3])
        p3l2.flip(p3l2, [3, 2, 1, 5, 4])
        l2p3.flip(l2p3, [2, 1, 5, 4, 3])

    _l3p2("l3p2", "p3l2", "p2l3", "l2p3", _generate_layouts_l3p2())
    _l3p2("l3s2", "p3s2", "s2l3", "s2p3", _generate_layouts_l3s2())

    caption6 = [
        C.TopLeft,
        C.TopRight,
        C.MiddleLeft,
        C.MiddleRight,
        C.BottomLeft,
        C.BottomRight,
    ]
    l6 = Layout("l6").generate(6, _generate_layouts_l6(), caption6)
    p6 = l6.transpose("p6", [1, 3, 5, 2, 4, 6])
    l6.flip(l6, [2, 1, 4, 3, 6, 5])
    l6.flip(l6, [5, 6, 3, 4, 1, 2])
    p6.flip(p6, [3, 2, 1, 6, 5, 4])
    p6.flip(p6, [4, 5, 6, 1, 2, 3])

    p6b = Layout("p6b").generate(6, _generate_layouts_p6b(), caption6)
    p6b.flip(p6b, [5, 6, 3, 4, 1, 2])
    p6b.flip(p6b, [2, 1, 4, 3, 6, 5])
    p6b.flip(p6, [1, 2, 3, 4, 5, 6])

    # TODO: square slot variants?
    l3p3a = Layout("l3p3a").generate(6, _generate_layouts_l3p3a(), caption6)
    l3p3c = Layout("l3p3c").generate(6, _generate_layouts_l3p3c(), caption6)
    l3p3b = l3p3a.flip_h("l3p3b", [2, 1, 4, 3, 6, 5])
    l3p3d = l3p3c.flip_h("l3p3d", [2, 1, 4, 3, 6, 5])
    l3p3a.flip(l3p3a, [5, 6, 3, 4, 1, 2])
    l3p3b.flip(l3p3b, [5, 6, 3, 4, 1, 2])

    l3p3c.flip(l3p3c, [5, 6, 3, 4, 1, 2])
    l3p3d.flip(l3p3d, [5, 6, 3, 4, 1, 2])

    l3p3a.flip(l3p3c, [1, 2, 4, 3, 5, 6])
    l3p3b.flip(l3p3d, [1, 2, 4, 3, 5, 6])

    p3l3a = l3p3a.transpose("p3l3a", [1, 3, 5, 2, 4, 6])
    p3l3c = l3p3c.transpose("p3l3c", [1, 3, 5, 2, 4, 6])
    p3l3b = p3l3a.flip_v("p3l3b", [4, 5, 6, 1, 2, 3])
    p3l3d = p3l3c.flip_v("p3l3d", [4, 5, 6, 1, 2, 3])
    p3l3a.flip(p3l3a, [3, 2, 1, 6, 5, 4])
    p3l3b.flip(p3l3b, [3, 2, 1, 6, 5, 4])

    p3l3c.flip(p3l3c, [3, 2, 1, 6, 5, 4])
    p3l3d.flip(p3l3d, [3, 2, 1, 6, 5, 4])

    p3l3a.flip(p3l3c, [1, 5, 3, 4, 2, 6])
    p3l3b.flip(p3l3d, [1, 5, 3, 4, 2, 6])

    p3x3 = Layout("p3x3").generate(9, _generate_layouts_grid(3, 3, "P"))
    p3x4 = Layout("p3x4").generate(12, _generate_layouts_grid(3, 4, "P"))
    p4x4 = Layout("p4x4").generate(16, _generate_layouts_grid(4, 4, "P"))
    l3x3 = Layout("l3x3").generate(9, _generate_layouts_grid(3, 3, "L"))
    l3x4 = Layout("l3x4").generate(12, _generate_layouts_grid(3, 4, "L"))
    l3x5 = Layout("l3x5").generate(15, _generate_layouts_grid(3, 5, "L"))
    l4x4 = Layout("l4x4").generate(16, _generate_layouts_grid(4, 4, "L"))

    g3x3 = Layout("g3x3").generate(9, _generate_layouts_grid(3, 3, "S"))
    g7x10 = Layout("g7x10").generate(70, _generate_layouts_grid(7, 10, "S"))

    return Layout.all_layouts


def _generate_layouts_l1() -> GenSlots:
    def l1_slots(check, W, G, L, P, S):  # type: ignore
        w1 = W
        h1 = w1 / L
        x1 = W * 0
        y1 = (W - h1) / 2
        return locals()

    return {"default": l1_slots}


def _generate_layouts_s1() -> GenSlots:
    def s1_slots(check, W, G, L, P, S):  # type: ignore
        w1 = W
        h1 = W
        x1 = W * 0
        y1 = W * 0
        return locals()

    return {"default": s1_slots}


def _generate_layouts_l2() -> GenSlots:
    def l2_slots(check, W, G, L, P, S):  # type: ignore
        h1 = h2 = (W - G) / 2
        w1 = w2 = h1 * L
        y1 = W * 0
        y2 = h1 + G
        x1 = x2 = (W - w1) / 2
        side1 = side2 = h1
        return locals()

    return {"default": l2_slots}


def _generate_layouts_s2a() -> GenSlots:
    def s2a_slots(check, W, G, L, P, S):  # type: ignore
        h1 = h2 = (W - G) / 2
        w1 = w2 = h1
        y1 = W * 0
        y2 = h1 + G
        x1 = x2 = (W - w1) / 2
        side1 = side2 = h1
        return locals()

    return {"default": s2a_slots}


def _generate_layouts_pll() -> GenSlots:
    def pll_slots(check, W, G, L, P, S):  # type: ignore
        w1 = w2 = w3 = (W - G) / 2
        h1 = w1 / P
        h2 = h3 = w2 / L
        x1 = W * 0
        x2 = x3 = w1 + G
        y1 = (W - h1) / 2
        y2 = (W - h2 - G - h3) / 2
        y3 = y2 + h2 + G
        bottom1 = bottom2 = (W - G) / 2
        return locals()

    def pll_slots_expanded(check, W, G, L, P, S):  # type: ignore
        check.constraint("w1 + G + w2 == W")
        check.constraint("w1 == h1 * P")
        check.constraint("h2 == (h1 - G) / 2")
        check.constraint("w2 == h2 * L")
        check.constraint("w3 == w2")
        check.constraint("h3 == h2")
        check.intermediate("h1 * P + G + h2 * L == W")
        check.intermediate("h1 * P + ((h1 - G) / 2) * L == W - G")
        check.intermediate("h1 * P + h1 * L / 2 - G * L / 2 == W - G")
        check.intermediate("h1 * P + h1 * L / 2 == W - G + G * L / 2")
        check.intermediate("h1 * (P + L / 2) == W + G * (L / 2 - 1)")
        h1 = (W + G * (L / 2 - 1)) / (P + L / 2)
        w1 = h1 * P
        h2 = h3 = (h1 - G) / 2
        w2 = w3 = h2 * L
        x1 = W * 0
        x2 = x3 = w1 + G
        y1 = (W - h1) / 2
        y2 = (W - h2 - G - h3) / 2
        y3 = y2 + h2 + G
        bottom1 = w1
        bottom2 = w2
        return locals()

    return {"default": pll_slots, "expanded": pll_slots_expanded}


def _generate_layouts_pss() -> GenSlots:
    def pss_slots(check, W, G, L, P, S):  # type: ignore
        w1 = w2 = w3 = (W - G) / 2
        h1 = w1 / P
        h2 = h3 = w2 / S
        x1 = W * 0
        x2 = x3 = w1 + G
        y1 = (W - h1) / 2
        y2 = (W - h2 - G - h3) / 2
        y3 = y2 + h2 + G
        bottom1 = bottom2 = (W - G) / 2
        return locals()

    def pss_slots_expanded(check, W, G, L, P, S):  # type: ignore
        check.constraint("w1 + G + w2 == W")
        check.constraint("w1 == h1 * P")
        check.constraint("h2 == (h1 - G) / 2")
        check.constraint("w2 == h2")
        check.constraint("w3 == w2")
        check.constraint("h3 == h2")
        check.intermediate("(h1 * P) + G + ((h1 - G) / 2) == W")
        check.intermediate("h1 * P + h1 / 2 - G / 2 == W - G")
        check.intermediate("h1 * P + h1 / 2 == W - G / 2")
        check.intermediate("2 * h1 * P + h1 == 2 * W - G")
        check.intermediate("h1 * (2 * P + 1) == 2 * W - G")
        h1 = (2 * W - G) / (2 * P + 1)
        w1 = h1 * P
        h2 = h3 = (h1 - G) / 2
        w2 = w3 = h2
        x1 = W * 0
        x2 = x3 = w1 + G
        y1 = y2 = (W - h1) / 2
        y3 = y2 + h2 + G
        bottom1 = w1
        bottom2 = w2
        return locals()

    return {"default": pss_slots, "expanded": pss_slots_expanded}


def _generate_layouts_l3() -> GenSlots:
    def l3_slots(check, W, G, L, P, S):  # type: ignore
        h1 = h2 = h3 = (W - G * 2) / 3
        w1 = w2 = w3 = h1 * L
        x1 = x2 = x3 = (W - w1) / 2
        y1 = W * 0
        y2 = y1 + h1 + G
        y3 = y2 + h2 + G
        side1 = side2 = side3 = h1
        return locals()

    return {"default": l3_slots}


def _generate_layouts_l2a() -> GenSlots:
    def l2a_slots(check, W, G, L, P, S):  # type: ignore
        # size as per l3, but with nicer use of whitespace for 2 slots
        h1 = h2 = (W - G * 2) / 3
        w1 = w2 = h1 * L
        x1 = x2 = (W - w1) / 2
        # slot 1 evenly spaced in top half of the page
        # slot 2 at top of bottom half of the page
        v = (W / 2 - h1) / 2
        y1 = W * 0 + v
        y2 = W / 2
        side1 = side2 = h1
        return locals()

    return {"default": l2a_slots}


def _generate_layouts_l4() -> GenSlots:
    def l4_slots(check, W, G, L, P, S):  # type: ignore
        w1 = w2 = w3 = w4 = (W - G) / 2
        h1 = h2 = h3 = h4 = w1 / L
        x1 = x3 = W * 0
        x2 = x4 = x1 + w1 + G
        y1 = y2 = (W - h1 - G - h3) / 2
        y3 = y4 = y1 + h1 + G
        side1 = side2 = bottom1 = bottom2 = w1
        return locals()

    return {"default": l4_slots}


def _generate_layouts_s4() -> GenSlots:
    def s4_slots(check, W, G, L, P, S):  # type: ignore
        h1 = h2 = h3 = h4 = (W - G) / 2
        w1 = w2 = w3 = w4 = h1
        y1 = y2 = W * 0
        y3 = y4 = y1 + h1 + G
        x1 = x3 = W * 0
        x2 = x4 = x1 + w1 + G
        side1 = side2 = bottom1 = bottom2 = h1
        return locals()

    return {"default": s4_slots}


def _generate_layouts_lplp() -> GenSlots:
    def lplp_slots(check, W, G, L, P, S):  # type: ignore
        h2 = h4 = (W - G) / 2
        w2 = w4 = h2 * P
        w1 = w3 = h2
        h1 = h3 = w1 / L
        x1 = x3 = W * 0
        x2 = x4 = x1 + w1 + G
        y2 = W * 0
        y4 = y2 + h2 + G
        y1 = (W - h1 - G - h3) / 2
        y3 = y1 + h1 + G
        side1 = side2 = bottom1 = bottom2 = h2
        return locals()

    def lplp_slots_expanded(check, W, G, L, P, S):  # type: ignore
        R = 1 / (1 + P / L)
        w1 = w3 = (W - G) * R
        w2 = w4 = (W - G) * (1 - R)
        h1 = h3 = w1 / L
        h2 = h4 = w2 / P
        x1 = x3 = W * 0
        x2 = x4 = x1 + w1 + G
        y1 = y2 = (W - G - h1 - h3) / 2
        y3 = y4 = y1 + h1 + G
        side1 = side2 = (W - G) / 2
        bottom1 = w1
        bottom2 = w2
        return locals()

    return {"default": lplp_slots, "expanded": lplp_slots_expanded}


def _generate_layouts_lsls() -> GenSlots:
    def lsls_slots(check, W, G, L, P, S):  # type: ignore
        h2 = h4 = (W - G) / 2
        w2 = w4 = h2 * S
        w1 = w3 = h2
        h1 = h3 = w1 / L
        x1 = x3 = W * 0
        x2 = x4 = x1 + w1 + G
        y2 = W * 0
        y4 = y2 + h2 + G
        y1 = (W - h1 - G - h3) / 2
        y3 = y1 + h1 + G
        side1 = side2 = bottom1 = bottom2 = h2
        return locals()

    def lsls_slots_expanded(check, W, G, L, P, S):  # type: ignore
        R = 1 / (1 + S / L)
        w1 = w3 = (W - G) * R
        w2 = w4 = (W - G) * (1 - R)
        h1 = h3 = w1 / L
        h2 = h4 = w2 / S
        x1 = x3 = W * 0
        x2 = x4 = x1 + w1 + G
        y1 = y2 = (W - G - h1 - h3) / 2
        y3 = y4 = y1 + h1 + G
        side1 = side2 = (W - G) / 2
        bottom1 = w1
        bottom2 = w2
        return locals()

    return {"default": lsls_slots, "expanded": lsls_slots_expanded}


def _generate_layouts_lppl() -> GenSlots:
    def lppl_slots(check, W, G, L, P, S):  # type: ignore
        w1 = w4 = (W - G) / 2
        h1 = h4 = w1 / L
        h2 = h3 = (W - G) / 2
        w2 = w3 = h2 * P
        x1 = y2 = W * 0
        x2 = x4 = y3 = y4 = w1 + G
        y1 = W - h1 - G - h3
        x3 = W - w4 - G - w3
        side1 = side2 = bottom1 = bottom2 = w1
        return locals()

    def lppl_slots_expanded(check, W, G, L, P, S):  # type: ignore
        w1 = w4 = (W - G) / (1 + P)
        h1 = h4 = w1 / L
        h2 = h3 = (W - G) / (1 + P)
        w2 = w3 = h2 * P
        x1 = x3 = y1 = y2 = W * 0
        x2 = w1 + G
        x4 = w3 + G
        y3 = h1 + G
        y4 = h2 + G
        return locals()

    return {"default": lppl_slots, "expanded": lppl_slots_expanded}


def _generate_layouts_l3p2() -> GenSlots:
    def l3p2_slots(check, W, G, L, P, S):  # type: ignore
        w1 = w2 = w3 = min((W - G) / 2, L * (W - 2 * G) / 3)
        h1 = h2 = h3 = w1 / L
        h4 = h5 = (h1 + G + h2 + G + h3 - G) / 2
        w4 = w5 = h4 * P
        x1 = x2 = x3 = (W - G) / 2 - w1
        x4 = x5 = x1 + w1 + G
        y1 = y4 = (W - h4 - G - h5) / 2
        y2 = y1 + h1 + G
        y3 = y2 + h2 + G
        y5 = y4 + h4 + G
        bottom1 = bottom2 = (W - G) / 2
        return locals()

    def l3p2_slots_expanded(check, W, G, L, P, S):  # type: ignore
        w1 = w2 = w3 = min(L * (W - 2 * G) / 3, (W - G) / (1 + P))
        h1 = h2 = h3 = w1 / L
        h4 = h5 = (h1 + G + h2 + G + h3 - G) / 2
        w4 = w5 = h4 * P
        x1 = x2 = x3 = (W - w1 - G - w4) / 2
        x4 = x5 = x1 + w1 + G
        y1 = y4 = (W - h4 - G - h5) / 2
        y2 = y1 + h1 + G
        y3 = y2 + h2 + G
        y5 = y4 + h4 + G
        bottom1 = x1 + w1
        bottom2 = W - G - bottom1
        return locals()

    return {"default": l3p2_slots, "expanded": l3p2_slots_expanded}


def _generate_layouts_l3s2() -> GenSlots:
    def l3s2_slots(check, W, G, L, P, S):  # type: ignore
        w1 = w2 = w3 = min((W - G) / 2, L * (W - 2 * G) / 3)
        h1 = h2 = h3 = w1 / L
        h4 = h5 = (h1 + G + h2 + G + h3 - G) / 2
        w4 = w5 = h4 * S
        x1 = x2 = x3 = (W - G) / 2 - w1
        x4 = x5 = x1 + w1 + G
        y1 = y4 = (W - h4 - G - h5) / 2
        y2 = y1 + h1 + G
        y3 = y2 + h2 + G
        y5 = y4 + h4 + G
        bottom1 = bottom2 = (W - G) / 2
        return locals()

    def l3s2_slots_expanded(check, W, G, L, P, S):  # type: ignore
        # a) h1 + G + h2 + G + h3 <= W
        # w1 = h1 * L
        # w1 = L * (W - 2 * G) / 3
        # b) w1 + G + w4 <= W
        # h4 + G + h5 == h1 + G + h2 + G + h3
        # h4 == (h1 * 3 + G) / 2
        # w4 == (3 * w1 / L + G) / 2
        # w1 + G + (3 * w1 / L + G) / 2 <= W
        # w1 + (3 * w1 / L + G) / 2 <= W - G
        # 2 * w1 + 3 * w1 / L + G <= 2 * (W - G)
        # 2 * w1 + 3 * w1 / L <= 2 * (W - G) - G
        # w1 * (2 * L + 3) <= 2 * L * (W - G) - G * L
        # w1 <= (2 * L * (W - G) - G * L) / (2 * L + 3)
        w1 = w2 = w3 = min(L * (W - 2 * G) / 3, (2 * L * (W - G) - G * L) / (2 * L + 3))
        h1 = h2 = h3 = w1 / L
        h4 = h5 = (h1 + G + h2 + G + h3 - G) / 2
        w4 = w5 = h4 * S
        x1 = x2 = x3 = (W - w1 - G - w4) / 2
        x4 = x5 = x1 + w1 + G
        y1 = y4 = (W - h4 - G - h5) / 2
        y2 = y1 + h1 + G
        y3 = y2 + h2 + G
        y5 = y4 + h4 + G
        bottom1 = x1 + w1
        bottom2 = W - G - bottom1
        return locals()

    return {"default": l3s2_slots, "expanded": l3s2_slots_expanded}


def _generate_layouts_l6() -> GenSlots:
    def l6_slots(check, W, G, L, P, S):  # type: ignore
        w1 = w2 = w3 = w4 = w5 = w6 = min((W - G) / 2, L * (W - 2 * G) / 3)
        h1 = h2 = h3 = h4 = h5 = h6 = w1 / L
        x1 = x3 = x5 = (W - w1 - G - w2) / 2
        x2 = x4 = x6 = x1 + w1 + G
        y1 = y2 = (W - h1 - G - h2 - G - h3) / 2
        y3 = y4 = y1 + h1 + G
        y5 = y6 = y3 + h3 + G
        bottom1 = bottom2 = (W - G) / 2
        side1 = side2 = side3 = (W - 2 * G) / 3
        return locals()

    return {"default": l6_slots}


def _generate_layouts_p6b() -> GenSlots:
    def p6b_slots(check, W, G, L, P, S):  # type: ignore
        w1 = w2 = w3 = w4 = w5 = w6 = min((W - G) / 2, P * (W - 2 * G) / 3)
        h1 = h2 = h3 = h4 = h5 = h6 = w1 / P
        x1 = x3 = x5 = (W - w1 - G - w2) / 2
        x2 = x4 = x6 = x1 + w1 + G
        y1 = y2 = (W - h1 - G - h2 - G - h3) / 2
        y3 = y4 = y1 + h1 + G
        y5 = y6 = y3 + h3 + G
        bottom1 = bottom2 = (W - G) / 2
        side1 = side2 = side3 = (W - 2 * G) / 3
        return locals()

    return {"default": p6b_slots}


def _generate_layouts_l3p3a() -> GenSlots:
    def l3p3a_slots(check, W, G, L, P, S):  # type: ignore
        h1 = h2 = h3 = h4 = h5 = h6 = (W - G * 2) / 3
        w1 = w3 = w5 = h1 * L
        w2 = w4 = w6 = h2 * P
        x1 = x3 = x5 = (W - w1 - G - w2) / 2
        x2 = x4 = x6 = x1 + w1 + G
        y1 = y2 = W * 0
        y3 = y4 = y1 + h1 + G
        y5 = y6 = y3 + h3 + G
        bottom1 = x1 + w1
        bottom2 = W - G - bottom1
        side1 = side2 = side3 = (W - 2 * G) / 3
        return locals()

    return {"default": l3p3a_slots}


def _generate_layouts_l3p3c() -> GenSlots:
    def l3p3c_slots(check, W, G, L, P, S):  # type: ignore
        h1 = h2 = h3 = h4 = h5 = h6 = (W - G * 2) / 3
        w1 = w4 = w5 = h1 * L
        w2 = w3 = w6 = h2 * P
        x1 = x3 = x5 = (W - w1 - G - w2) / 2
        x2 = x6 = x1 + w1 + G
        x4 = x3 + w3 + G
        y1 = y2 = W * 0
        y3 = y4 = y1 + h1 + G
        y5 = y6 = y3 + h3 + G
        side1 = side2 = side3 = (W - 2 * G) / 3
        return locals()

    return {"default": l3p3c_slots}


def _generate_layouts_grid(cols: int, rows: int, aspect_: str) -> GenSlots:
    def grid_slots(check, W, G, L, P, S):  # type: ignore
        aspect = locals()[aspect_]
        if (cols * aspect) > (rows / aspect):
            w = (W - (cols - 1) * G) / cols
            h = w / aspect
            x0 = 0
            y0 = (W - (rows * (h + G) - G)) / 2
        else:
            h = (W - (rows - 1) * G) / rows
            w = h * aspect
            x0 = (W - (cols * (w + G) - G)) / 2
            y0 = 0

        grid = {}
        k = 0
        for i in range(rows):
            for j in range(cols):
                k += 1
                grid[f"w{k}"] = w
                grid[f"h{k}"] = h
                grid[f"y{k}"] = y0 + i * (h + G)
                grid[f"x{k}"] = x0 + j * (w + G)

        return grid

    return {"default": grid_slots}


ALL_LAYOUTS = generate_layouts()
_LAYOUT_DICT = {layout.layout_id: layout for layout in ALL_LAYOUTS}

LAYOUT_LOOKUP = defaultdict(list)
for layout in ALL_LAYOUTS:
    key = "".join(s.orientation[0] for s in layout.slots)
    LAYOUT_LOOKUP[key].append(layout)
# print(', '.join(f'{k}: {len(v)}' for k, v in LAYOUT_LOOKUP.items()))

# make p1a the default
LAYOUT_LOOKUP["p"].reverse()


def get_layout_by_id(layout_id: str) -> Layout:
    return _LAYOUT_DICT[layout_id]


def get_cover_layout(cover_photo_orientation: str | None) -> str:
    if cover_photo_orientation is None or cover_photo_orientation == "landscape":
        return "l1"

    if cover_photo_orientation == "square":
        return "s1"

    return "p1"
