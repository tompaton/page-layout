# ruff: noqa: N803, N806

import functools
from collections.abc import Callable
from fractions import Fraction
from typing import ClassVar, cast

from page_boxes.box import Box
from page_boxes.rect import Rect

from page_layout.aspect import ASPECTS, DEFAULT_ASPECT, get_aspect
from page_layout.caption import Caption, _Caption

N = Fraction
Slots = dict[str, N]
ColWidths = list[N]
SlotPerm = list[int]


LayoutParamsKey = tuple[Fraction, str | None, Fraction | None, str | None]


class LayoutParams:
    def __init__(
        self,
        page_width: Fraction,
        page_mode: str | None = "default",
        gutter: Fraction | None = Fraction(0),
        aspect: str | None = DEFAULT_ASPECT,
        caption_position: str = "off",
        caption_columns: int = 1,
    ) -> None:
        self.page_width = page_width
        self.page_mode = page_mode
        self.gutter = gutter
        self.aspect = aspect
        self.caption_position = caption_position
        self.caption_columns = caption_columns

    def key(self) -> LayoutParamsKey:
        return (self.page_width, self.page_mode, self.gutter, self.aspect)


class LayoutSlot:
    def __init__(self, index: int, layout: "Layout", caption: _Caption) -> None:
        self.index = index
        self.layout = layout
        self.caption = caption

        rect = self.get_rect(LayoutParams(Fraction(1)))
        self.orientation = rect.orientation
        self.highlight = rect.area >= Fraction(1, 3)
        self.diminish = rect.area <= Fraction(1, 6)

    def get_rect(self, params: LayoutParams) -> Box:
        return self.layout.get_rect(self.index, params)


MAX_SLOTS = 6


class LayoutLinkError(Exception):
    pass


class LayoutCheckError(Exception):
    pass


class Check:
    """
    Accumulate constraints and intermediate derivation steps from call to
    gen_slots and verify they hold for final slot coordinates.
    """

    def __init__(self) -> None:
        self.checks: list[str] = []

    def constraint(self, check: str) -> None:
        self.checks.append(check)

    intermediate = constraint

    def verify(self, slots: Slots) -> None:
        for check in self.checks:
            if not eval(check, slots):  # noqa: S307
                used = check.split()
                raise LayoutCheckError(check + " " + repr({k: slots[k] for k in slots if k in used}))


GenSlotsFn = Callable[[Check, N, N, N, N, N], Slots]
GenSlots = dict[str, GenSlotsFn]


class Layout:
    all_layouts: ClassVar[list["Layout"]] = []

    def __init__(self, layout_id: str) -> None:
        Layout.all_layouts.append(self)
        self.seq = len(Layout.all_layouts)

        self.layout_id = layout_id

        self.__get_rect: dict[LayoutParamsKey, Slots] = {}

        self.gen_slots: GenSlots = {}
        self.slots: list[LayoutSlot] = []

        # list of (layout_id, slot_permutation)
        self.alt_layouts: list[tuple[str, SlotPerm]] = []

    def generate(self, count: int, gen_slots: GenSlots, captions: list[_Caption] | None = None) -> "Layout":
        self.gen_slots = gen_slots

        def caption(slot: int) -> _Caption:
            if captions:
                return captions[slot - 1]

            return Caption.Blank

        self.slots = [LayoutSlot(i, self, caption(i)) for i in range(1, count + 1)]

        self._check()

        return self

    def flip_h(self, layout_id: str, perm_other: SlotPerm) -> "Layout":
        def flip_var(W: N, slots: Slots, var: str) -> N:
            if var[0] == "x":
                return cast(N, W - slots[var] - slots["w" + var[1:]])

            return slots[var]

        flipped = self._flip(layout_id, perm_other, flip_var, "flip_h")
        self.flip(flipped, perm_other)
        return flipped

    def flip_v(self, layout_id: str, perm_other: SlotPerm) -> "Layout":
        def flip_var(W: N, slots: Slots, var: str) -> N:
            if var[0] == "y":
                return cast(N, W - slots[var] - slots["h" + var[1:]])

            return slots[var]

        flipped = self._flip(layout_id, perm_other, flip_var, "flip_v")
        self.flip(flipped, perm_other)
        return flipped

    def transpose(self, layout_id: str, perm_other: SlotPerm) -> "Layout":
        var_map = {"x": "y", "y": "x", "w": "h", "h": "w"}

        def transpose_var(_W: N, slots: Slots, var: str) -> N:
            return slots[var_map[var[0]] + var[1:]]

        return self._flip(layout_id, perm_other, transpose_var, "transpose")

    def _flip(
        self,
        layout_id: str,
        perm_other: SlotPerm,
        flip_var: Callable[..., N],
        flip_caption: str,
    ) -> "Layout":
        perm_self = self._invert_perm(perm_other)

        def _flip_(original_gen_slots: GenSlotsFn) -> GenSlotsFn:
            @functools.wraps(original_gen_slots)
            def wrapper(_check, W, G, L, P, S):  # type: ignore
                # constraints only hold for the first variant
                new_check = Check()
                slots = original_gen_slots(new_check, W, G, L, P, S)
                perm = {}
                for var in slots:
                    if var[0] in "xywh":
                        target = var[0] + str(perm_self[int(var[1:]) - 1])
                        perm[target] = flip_var(W, slots, var)

                    if var.startswith(("side", "bottom")):
                        perm.update(self._flip_caption(slots, var, flip_caption))

                return perm

            return wrapper

        new_gen_slots = {page_mode: _flip_(gen_slots) for page_mode, gen_slots in self.gen_slots.items()}

        new_captions = Caption.flip(flip_caption, [self.slots[i - 1].caption for i in perm_other])

        return Layout(layout_id).generate(self.num_slots, new_gen_slots, new_captions)

    @staticmethod
    def _flip_caption(slots: Slots, var: str, mode: str) -> Slots:
        if mode == "flip_h":
            if var.startswith("bottom"):
                col = int(var[-1])
                ncols = len([key for key in slots if key.startswith("bottom")])
                return {"bottom%d" % (ncols - col + 1): slots[var]}

            return {var: slots[var]}

        if mode == "flip_v":
            if var.startswith("side"):
                col = int(var[-1])
                ncols = len([key for key in slots if key.startswith("side")])
                return {"side%d" % (ncols - col + 1): slots[var]}

            return {var: slots[var]}

        if mode == "transpose":
            if var.startswith("side"):
                return {var.replace("side", "bottom"): slots[var]}
            if var.startswith("bottom"):
                return {var.replace("bottom", "side"): slots[var]}

        msg = "unhandled mode/var"
        raise RuntimeError(msg)  # pragma: nocover

    def _check(self) -> None:
        for aspect in ASPECTS:
            for slot in self.slots:
                for page_mode, gen_slots in self.gen_slots.items():
                    # check main aspect ratio
                    layout_params = LayoutParams(Fraction(1), page_mode=page_mode, aspect=aspect.code)
                    main_rect = slot.get_rect(layout_params)
                    if not aspect.valid(main_rect.width, main_rect.height):
                        msg = f"bad gen_slots(W=1) {gen_slots!r}"
                        raise ValueError(msg)

                    # check gutter aspect ratio
                    layout_params = LayoutParams(
                        Fraction(0),
                        page_mode=page_mode,
                        gutter=Fraction(1),
                        aspect=aspect.code,
                    )
                    gutter_rect = slot.get_rect(layout_params)
                    if gutter_rect.height and not aspect.valid(gutter_rect.width, gutter_rect.height):
                        msg = f"bad gen_slots(G=1) {gen_slots!r}"
                        raise ValueError(msg)

                    # trigger verify with W and G set
                    layout_params = LayoutParams(
                        Fraction(10),
                        page_mode=page_mode,
                        gutter=Fraction(1),
                        aspect=aspect.code,
                    )
                    slot.get_rect(layout_params)

                    # TODO: check slots are within page bounds, no overlaps

    @property
    def num_slots(self) -> int:
        return len(self.slots)

    def get_rect(self, index: int, params: LayoutParams) -> Box:
        slots = self._get_slots(params)

        return Box(
            slots["x%d" % index],
            slots["y%d" % index],
            slots["w%d" % index],
            slots["h%d" % index],
        )

    def _get_slots(self, params: LayoutParams) -> Slots:
        key = params.key()

        if key not in self.__get_rect:
            a = get_aspect(params.aspect)
            if params.page_mode is None:
                gen_slots = self.gen_slots["default"]
            else:
                gen_slots = self.gen_slots.get(params.page_mode) or self.gen_slots["default"]
            check = Check()
            if params.gutter is None:
                raise ValueError(params.gutter)
            W, G = params.page_width, params.gutter
            slots = gen_slots(check, W, G, a.L, a.P, a.S)
            slots.update(W=W, G=G, L=a.L, P=a.P, S=a.S)
            check.verify(slots)
            self.__get_rect[key] = slots
        else:
            slots = self.__get_rect[key]

        return slots

    def get_caption_columns(self, params: LayoutParams) -> Slots:
        slots = self._get_slots(params)

        cols = {key: value for key, value in slots.items() if key.startswith(("side", "bottom"))}

        cols.setdefault("side1", params.page_width)
        cols.setdefault("bottom1", params.page_width)

        return cols

    def get_column_widths(self, params: LayoutParams, even_odd: str) -> ColWidths:
        cols = self.get_caption_columns(params)
        pos = params.caption_position
        if pos == "bottom2":
            pos = "bottom"
        widths = [cols[key] for key in ["%s%d" % (pos, i + 1) for i in range(3)] if key in cols]
        if params.caption_columns != len(widths):
            return [params.page_width]

        if even_odd == "even" and pos == "side":
            return list(reversed(widths))

        return widths

    def flip(self, other: "Layout", perm_other: SlotPerm) -> None:
        if self.num_slots != other.num_slots:
            msg = f"{self.layout_id}<=>{other.layout_id} slot count"
            raise LayoutLinkError(msg)

        self.check_perm(other, perm_other)
        self.alt_layouts.append((other.layout_id, perm_other))

        if self != other:
            perm_self = self._invert_perm(perm_other)
            other.check_perm(self, perm_self)
            other.alt_layouts.append((self.layout_id, perm_self))

    @staticmethod
    def _invert_perm(perm: SlotPerm) -> SlotPerm:
        return [i for p, i in sorted([(p, i + 1) for i, p in enumerate(perm)])]

    def check_perm(self, other: "Layout", perm_other: SlotPerm) -> None:
        for i, p in enumerate(perm_other):
            if self.slots[p - 1].orientation != other.slots[i].orientation:
                raise LayoutLinkError(
                    "%s<=>%s slot %d<=>%d orientation %s!=%s %r"
                    % (
                        self.layout_id,
                        other.layout_id,
                        p,
                        i + 1,
                        self.slots[p - 1].orientation[0].upper(),
                        other.slots[i].orientation[0].upper(),
                        perm_other,
                    )
                )

    def is_valid(self, num_photos: int, photo_orientations: list[str]) -> bool:
        """
        return True if layout can be used at that point in the list of photos.
        """

        # not enough photos
        if num_photos < self.num_slots:
            return False

        # orientation doesn't match
        if any(
            orientation != slot.orientation for orientation, slot in zip(photo_orientations, self.slots, strict=False)
        ):
            return False

        return True

    def score(self, topics: list[str], last_topic: str | None, logger: Callable[..., None]) -> float:
        # keep topics together and consume as many photos as possible

        score = 0.0

        # + if first photo is a new topic
        if topics[0] != last_topic:
            score += 1.0

        # - if multiple topics on page
        score -= len(set(topics))

        # + if multiple photos on page with same topic
        score += sum(topic == topics[0] for topic in topics)

        # HACK: want to use P1a layout in preference to P1 layout, so
        # zero out the unwanted score
        if self.layout_id == "p1":
            score = 0.0

        # and if p1 and p1a both have zero score, prefer p1a
        if self.layout_id == "p1a":
            score += 0.1

        logger(" layout_id=%s score=%f topics=%r", self.layout_id, score, topics)

        return score

    def bounding_box(self, params: LayoutParams) -> Rect:
        # don't scale the p1a layout, it should be smaller than the page
        if self.layout_id == "p1a":
            return Rect("1.0", "1.0")

        if self.layout_id == "l2a":
            l3 = next(layout for layout in Layout.all_layouts if layout.layout_id == "l3")
            return l3.bounding_box(params)

        rects = [slot.get_rect(params) for slot in self.slots]

        left = min(rect.left for rect in rects)
        top = min(rect.top for rect in rects)
        right = max(rect.left + rect.width for rect in rects)
        bottom = max(rect.top + rect.height for rect in rects)

        # bounding box must remain symmetrical to ensure off center layouts are
        # scaled correctly
        center = params.page_width / 2
        return Rect(
            2 * max(right - center, center - left) / params.page_width,
            2 * max(bottom - center, center - top) / params.page_width,
        )
