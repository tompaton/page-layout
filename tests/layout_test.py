# ruff: noqa: N803
# pylint: disable=redefined-outer-name,protected-access

from fractions import Fraction
from typing import Any, cast
from unittest.mock import Mock, call

import pytest

from page_layout import layout_defs
from page_layout.aspect import DEFAULT_ASPECT
from page_layout.caption import Caption as C  # noqa
from page_layout.layout import (
    Check,
    Layout,
    LayoutCheckError,
    LayoutLinkError,
    LayoutParams,
    LayoutSlot,
    Slots,
)


# the to_dict method has been removed, but keeping the tests
def to_dict(layout: Layout, params: LayoutParams, params_dict: dict[str, Any], utilisation: int) -> dict[str, Any]:
    d: dict[str, Any] = {"layout_id": layout.layout_id, "utilisation": utilisation}

    d.update(params_dict)

    d["slots"] = [to_dict_slot(slot, params) for slot in layout.slots]
    d["alt"] = [{"layout_id": alt[0], "permutation": alt[1]} for alt in layout.alt_layouts]

    slots = layout.get_caption_columns(params)
    d["caption_columns"] = {
        pos: [f"{cast(float, slots[key]):.2f}" for key in ["%s%d" % (pos, i + 1) for i in range(3)] if key in slots]
        for pos in ("side", "bottom")
    }

    return d


def to_dict_slot(slot: LayoutSlot, params: LayoutParams) -> dict[str, Any]:
    rect = slot.get_rect(params)
    return {
        "top": float(rect.top),
        "left": float(rect.left),
        "width": float(rect.width),
        "height": float(rect.height),
        "caption_id": slot.caption.caption_id,
        "orientation": slot.orientation,
        "highlight": slot.highlight,
        "diminish": slot.diminish,
    }


def test_layoutparams_key() -> None:
    assert LayoutParams(Fraction(100)).key() == (
        Fraction(100),
        "default",
        Fraction(0),
        DEFAULT_ASPECT,
    )


@pytest.fixture
def l1() -> Layout:
    return Layout("l1").generate(1, layout_defs._generate_layouts_l1())


@pytest.fixture
def s1() -> Layout:
    return Layout("s1").generate(1, layout_defs._generate_layouts_s1())


def test_layout_to_dict_l1(l1: Layout) -> None:
    params = LayoutParams(Fraction(100), page_mode=None)
    assert to_dict(l1, params, {"param": "value"}, 25) == {
        "layout_id": "l1",
        "utilisation": 25,
        "param": "value",
        "slots": [
            {
                "top": 12.5,
                "left": 0.0,
                "width": 100.0,
                "height": 75.0,
                "caption_id": "Blank",
                "orientation": "landscape",
                "highlight": True,
                "diminish": False,
            }
        ],
        "alt": [],
        "caption_columns": {"bottom": ["100.00"], "side": ["100.00"]},
    }


@pytest.fixture
def pll() -> Layout:
    pll = Layout("pll").generate(3, layout_defs._generate_layouts_pll(), [C.Left, C.TopRight, C.BottomRight])
    lpp = pll.transpose("lpp", [1, 2, 3])
    llp = pll.flip_h("llp", [2, 3, 1])
    ppl = lpp.flip_v("ppl", [2, 3, 1])
    pll.flip(pll, [1, 3, 2])
    llp.flip(llp, [2, 1, 3])
    lpp.flip(lpp, [1, 3, 2])
    ppl.flip(ppl, [2, 1, 3])
    return pll


@pytest.fixture
def lpp() -> Layout:
    pll = Layout("pll").generate(3, layout_defs._generate_layouts_pll(), [C.Left, C.TopRight, C.BottomRight])
    return pll.transpose("lpp", [1, 2, 3])


def test_layout_to_dict_pll(pll: Layout) -> None:
    params = LayoutParams(Fraction(100))
    assert to_dict(pll, params, {"param": "value"}, 25) == {
        "layout_id": "pll",
        "utilisation": 25,
        "param": "value",
        "slots": [
            {
                "top": 16.666666666666668,
                "left": 0.0,
                "width": 50.0,
                "height": 66.66666666666667,
                "caption_id": "Left",
                "orientation": "portrait",
                "highlight": True,
                "diminish": False,
            },
            {
                "top": 12.5,
                "left": 50.0,
                "width": 50.0,
                "height": 37.5,
                "caption_id": "TopRight",
                "orientation": "landscape",
                "highlight": False,
                "diminish": False,
            },
            {
                "top": 50.0,
                "left": 50.0,
                "width": 50.0,
                "height": 37.5,
                "caption_id": "BottomRight",
                "orientation": "landscape",
                "highlight": False,
                "diminish": False,
            },
        ],
        "alt": [
            {"layout_id": "llp", "permutation": [2, 3, 1]},
            {"layout_id": "pll", "permutation": [1, 3, 2]},
        ],
        "caption_columns": {"bottom": ["50.00", "50.00"], "side": ["100.00"]},
    }


@pytest.mark.parametrize(
    ("slots_in", "var", "mode", "slots_out"),
    [
        ({"side1": 10}, "side1", "flip_h", {"side1": 10}),
        ({"side1": 10}, "side1", "flip_v", {"side1": 10}),
        ({"side1": 10}, "side1", "transpose", {"bottom1": 10}),
        ({"bottom1": 10}, "bottom1", "flip_h", {"bottom1": 10}),
        ({"bottom1": 10}, "bottom1", "flip_v", {"bottom1": 10}),
        ({"bottom1": 10}, "bottom1", "transpose", {"side1": 10}),
        ({"side1": 10, "side2": 20}, "side1", "flip_h", {"side1": 10}),
        ({"side1": 10, "side2": 20}, "side1", "flip_v", {"side2": 10}),
        ({"side1": 10, "side2": 20}, "side1", "transpose", {"bottom1": 10}),
        ({"side1": 10, "side2": 20}, "side2", "flip_h", {"side2": 20}),
        ({"side1": 10, "side2": 20}, "side2", "flip_v", {"side1": 20}),
        ({"side1": 10, "side2": 20}, "side2", "transpose", {"bottom2": 20}),
        ({"bottom1": 10, "bottom2": 20}, "bottom1", "flip_h", {"bottom2": 10}),
        ({"bottom1": 10, "bottom2": 20}, "bottom1", "flip_v", {"bottom1": 10}),
        ({"bottom1": 10, "bottom2": 20}, "bottom1", "transpose", {"side1": 10}),
        ({"bottom1": 10, "bottom2": 20}, "bottom2", "flip_h", {"bottom1": 20}),
        ({"bottom1": 10, "bottom2": 20}, "bottom2", "flip_v", {"bottom2": 20}),
        ({"bottom1": 10, "bottom2": 20}, "bottom2", "transpose", {"side2": 20}),
        ({"side1": 10, "side2": 20, "side3": 30}, "side1", "flip_h", {"side1": 10}),
        ({"side1": 10, "side2": 20, "side3": 30}, "side1", "flip_v", {"side3": 10}),
        (
            {"side1": 10, "side2": 20, "side3": 30},
            "side1",
            "transpose",
            {"bottom1": 10},
        ),
        ({"side1": 10, "side2": 20, "side3": 30}, "side2", "flip_h", {"side2": 20}),
        ({"side1": 10, "side2": 20, "side3": 30}, "side2", "flip_v", {"side2": 20}),
        (
            {"side1": 10, "side2": 20, "side3": 30},
            "side2",
            "transpose",
            {"bottom2": 20},
        ),
        ({"side1": 10, "side2": 20, "side3": 30}, "side3", "flip_h", {"side3": 30}),
        ({"side1": 10, "side2": 20, "side3": 30}, "side3", "flip_v", {"side1": 30}),
        (
            {"side1": 10, "side2": 20, "side3": 30},
            "side3",
            "transpose",
            {"bottom3": 30},
        ),
        (
            {"bottom1": 10, "bottom2": 20, "bottom3": 30},
            "bottom1",
            "flip_h",
            {"bottom3": 10},
        ),
        (
            {"bottom1": 10, "bottom2": 20, "bottom3": 30},
            "bottom1",
            "flip_v",
            {"bottom1": 10},
        ),
        (
            {"bottom1": 10, "bottom2": 20, "bottom3": 30},
            "bottom1",
            "transpose",
            {"side1": 10},
        ),
        (
            {"bottom1": 10, "bottom2": 20, "bottom3": 30},
            "bottom2",
            "flip_h",
            {"bottom2": 20},
        ),
        (
            {"bottom1": 10, "bottom2": 20, "bottom3": 30},
            "bottom2",
            "flip_v",
            {"bottom2": 20},
        ),
        (
            {"bottom1": 10, "bottom2": 20, "bottom3": 30},
            "bottom2",
            "transpose",
            {"side2": 20},
        ),
        (
            {"bottom1": 10, "bottom2": 20, "bottom3": 30},
            "bottom3",
            "flip_h",
            {"bottom1": 30},
        ),
        (
            {"bottom1": 10, "bottom2": 20, "bottom3": 30},
            "bottom3",
            "flip_v",
            {"bottom3": 30},
        ),
        (
            {"bottom1": 10, "bottom2": 20, "bottom3": 30},
            "bottom3",
            "transpose",
            {"side3": 30},
        ),
    ],
)
def test_layout_flip_caption(slots_in: Slots, var: str, mode: str, slots_out: Slots) -> None:
    assert Layout._flip_caption(slots_in, var, mode) == slots_out


def test_check() -> None:
    check = Check()
    check.constraint("A != B")
    check.constraint("A == B")
    with pytest.raises(LayoutCheckError) as ex_info:
        check.verify({"A": Fraction(1), "B": Fraction(2), "C": Fraction(3)})
    assert str(ex_info.value) == "A == B {'A': Fraction(1, 1), 'B': Fraction(2, 1)}"


def test_flip_layoutlinkerror_slot_count(l1: Layout, pll: Layout) -> None:
    with pytest.raises(LayoutLinkError) as ex_info:
        l1.flip(pll, [1])

    assert str(ex_info.value) == "l1<=>pll slot count"


def test_flip_layoutlinkerror_orientation(l1: Layout, s1: Layout) -> None:
    with pytest.raises(LayoutLinkError) as ex_info:
        l1.flip(s1, [1])

    assert str(ex_info.value) == "l1<=>s1 slot 1<=>1 orientation L!=S [1]"


def test_layout_is_valid(pll: Layout) -> None:
    assert not pll.is_valid(1, [])
    assert not pll.is_valid(3, ["square", "landscape", "landscape"])
    assert pll.is_valid(3, ["portrait", "landscape", "landscape"])


def test_bounding_box_l1(l1: Layout) -> None:
    params = LayoutParams(Fraction(100))
    assert l1.bounding_box(params).str1() == "Rect(width=1.0, height=0.75)"


def test_bounding_box_s1(s1: Layout) -> None:
    params = LayoutParams(Fraction(100))
    assert s1.bounding_box(params).str1() == "Rect(width=1.0, height=1.0)"


def test_bounding_box_pll(pll: Layout) -> None:
    params = LayoutParams(Fraction(100))
    assert pll.bounding_box(params).str1() == "Rect(width=1.0, height=0.75)"


def test_bounding_box_p1a() -> None:
    p1a = Layout("p1a")
    params = LayoutParams(Fraction(100))
    assert p1a.bounding_box(params).str1() == "Rect(width=1.0, height=1.0)"


@pytest.mark.parametrize(
    ("even_odd", "caption_columns", "caption_position", "result"),
    [
        ("even", 1, "off", [100]),
        ("even", 1, "bottom", [100]),
        ("even", 2, "bottom", [53, 47]),
        ("even", 1, "side", [100]),
        ("even", 2, "side", [100]),
        ("odd", 1, "off", [100]),
        ("odd", 1, "bottom", [100]),
        ("odd", 2, "bottom", [53, 47]),
        ("odd", 1, "side", [100]),
        ("odd", 2, "side", [100]),
    ],
)
def test_get_column_widths(
    pll: Layout,
    even_odd: str,
    caption_columns: int,
    caption_position: str,
    result: list[int],
) -> None:
    params = LayoutParams(
        Fraction(100),
        caption_position=caption_position,
        caption_columns=caption_columns,
        page_mode="expanded",
    )
    assert pll.get_column_widths(params, even_odd) == pytest.approx(result, abs=0.5)


@pytest.mark.parametrize(
    ("even_odd", "caption_columns", "caption_position", "result"),
    [
        ("even", 1, "off", [100]),
        ("even", 1, "bottom", [100]),
        ("even", 2, "bottom", [100]),
        ("even", 1, "side", [100]),
        ("even", 2, "side", [47, 53]),
        ("odd", 1, "off", [100]),
        ("odd", 1, "bottom", [100]),
        ("odd", 2, "bottom", [100]),
        ("odd", 1, "side", [100]),
        ("odd", 2, "side", [53, 47]),
    ],
)
def test_get_column_widths2(
    lpp: Layout,
    even_odd: str,
    caption_columns: int,
    caption_position: str,
    result: list[int],
) -> None:
    params = LayoutParams(
        Fraction(100),
        caption_position=caption_position,
        caption_columns=caption_columns,
        page_mode="expanded",
    )
    assert lpp.get_column_widths(params, even_odd) == pytest.approx(result, abs=0.5)


@pytest.mark.parametrize(
    ("layout_id", "topics", "last_topic", "score"),
    [
        ("pll", [""], "", 0.0),
        ("pll", ["A"], "", 1.0),
        ("pll", ["A", "A"], "", 2.0),
        ("pll", ["A", "B"], "", 0.0),
        ("p1", ["A"], "", 0.0),
        ("p1a", ["A"], "", 1.1),
    ],
)
def test_layout_score(layout_id: str, topics: list[str], last_topic: str, score: float) -> None:
    layout = Layout(layout_id)
    logger = Mock()
    assert layout.score(topics, last_topic, logger) == score
    assert logger.mock_calls == [call(" layout_id=%s score=%f topics=%r", layout_id, score, topics)]


def test_layout_check_bad_slot_w() -> None:
    layout = Layout("pll")

    def bad_slots(check, W, G, L, P, S):  # type: ignore
        # pylint: disable=W0613,W0641
        # w1 = h1 = W
        w1 = W * 3
        h1 = W * 4
        x1 = W * 0
        y1 = W * 0
        return locals()

    layout.gen_slots["default"] = bad_slots

    layout.slots.append(LayoutSlot(1, layout, C.Blank))

    with pytest.raises(ValueError, match=r"bad gen_slots\(W=1\).+") as ex_info:
        layout._check()

    assert str(ex_info.value) == f"bad gen_slots(W=1) {bad_slots!r}"


def test_layout_check_bad_slot_g() -> None:
    layout = Layout("pll")

    def bad_slots(check, W, G, L, P, S):  # type: ignore
        # pylint: disable=W0613,W0641
        # h1 = h2 = (W - G) / 2
        h1 = h2 = (W - G) / 2 + G
        # w1 = w2 = h1 * L
        w1 = w2 = h1 * L + G
        y1 = W * 0
        y2 = h1 + G
        x1 = x2 = (W - w1) / 2
        side1 = side2 = h1
        return locals()

    layout.gen_slots["default"] = bad_slots

    layout.slots.append(LayoutSlot(1, layout, C.Blank))
    layout.slots.append(LayoutSlot(2, layout, C.Blank))

    with pytest.raises(ValueError, match=r"bad gen_slots\(G=1\).+") as ex_info:
        layout._check()

    assert str(ex_info.value) == f"bad gen_slots(G=1) {bad_slots!r}"
