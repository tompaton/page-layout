# ruff: noqa: N803, N806


import pytest

from page_layout.layout_defs import get_layout_by_id
from page_layout.layout_option import LayoutOption, get_options


def test_layoutoption() -> None:
    assert repr(LayoutOption(100, get_layout_by_id("pll"))) == "<LayoutOption layout_id=pll>"


def test_num_slots() -> None:
    assert LayoutOption(100, get_layout_by_id("pll")).num_slots == 3


@pytest.mark.parametrize(
    ("orientations", "layouts"),
    [
        ([], [[]]),
        (["landscape"], [[], ["l1"]]),
        (["portrait"], [[], ["p1"]]),
        (["landscape", "landscape"], [[], ["l1"], ["l1"]]),
        (["portrait", "landscape", "landscape"], [[], ["l1"], ["l1"], ["p1", "pll"]]),
        (
            ["landscape", "portrait", "portrait", "landscape", "landscape"],
            [[], ["l1"], ["l1"], ["p1", "pll"], ["p1"], ["l1", "lpp"]],
        ),
    ],
)
def test_get_options(orientations: list[str], layouts: list[list[str]]) -> None:
    enabled_layouts = [
        get_layout_by_id("l1"),
        get_layout_by_id("p1"),
        get_layout_by_id("pll"),
        get_layout_by_id("lpp"),
    ]
    assert [
        [option.layout.layout_id for option in options] for options in get_options(enabled_layouts, orientations)
    ] == layouts
