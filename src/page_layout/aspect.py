from fractions import Fraction


class Aspect:
    def __init__(self, code: str, description: str, width: int, height: int) -> None:
        self.code = code
        self.description = description
        self.L = self.landscape = Fraction(width, height)
        self.P = self.portrait = Fraction(height, width)
        self.S = self.square = Fraction(1)

    def valid(self, width: float | Fraction, height: float | Fraction) -> bool:
        return width / height in (self.L, self.P, self.S)

    def crop(self, width: int, height: int, crop: str, orientation: str) -> tuple[Fraction, Fraction]:
        width2 = Fraction(width)
        height2 = Fraction(height)

        aspect = getattr(self, orientation)

        # biggest rectangle that will fit in image
        if crop == "inside":
            if height > width / aspect:
                height2 = width2 / aspect
            elif width > height * aspect:
                width2 = height2 * aspect

        # smallest rectangle that fits around image
        if crop == "outside":
            if height < width / aspect:
                height2 = width2 / aspect
            elif width < height * aspect:
                width2 = height2 * aspect

        return width2, height2


ASPECTS: list[Aspect] = [
    Aspect("9x21", "21 x 9 (cinema widescreen)", 21, 9),  # 2.333, 0.429
    Aspect("9x16", "16 x 9 (cinema)", 16, 9),  # 1.777, 0.5625
    Aspect("Phi", "Golden Ratio", 1597, 987),  # 1.618, 0.618
    Aspect("4x6", "4 x 6 (traditional print)", 3, 2),  # 1.5, 0.666
    Aspect("6x8", "6 x 8 (digital)", 4, 3),  # 1.333, 0.75
    Aspect("8x10", "8 x 10 (print enlargement)", 5, 4),  # 1.25, 0.8
    Aspect("1x1", "Square", 1, 1),  # 1.0, 1.0
]

DEFAULT_ASPECT = "6x8"


def get_aspect(code: str | None) -> Aspect:
    for aspect in ASPECTS:
        if aspect.code == code:
            return aspect
    raise ValueError(code)
