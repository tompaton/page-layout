class _Caption:
    def __init__(self, caption_id: str, normal: str) -> None:
        self.caption_id = caption_id
        self.normal = normal

        self.flip_h = caption_id
        self.flip_v = caption_id
        self.transpose = caption_id

        self.side: str | None = None
        self.bottom: str | None = None
        self.caption_column: dict[tuple[str, int], int] = {}

        self.columns()

    def __repr__(self) -> str:
        return f"_Caption(caption_id={self.caption_id})"

    def transform(self, flip_h: str, flip_v: str, transpose: str) -> "_Caption":
        self.flip_h = flip_h
        self.flip_v = flip_v
        self.transpose = transpose

        return self

    def columns(
        self,
        side: str | None = None,
        bottom: str | None = None,
        **caption_columns: int,
    ) -> "_Caption":
        self.side = side
        self.bottom = bottom

        self.caption_column = {
            (pos, cols): caption_columns.get("%s%d" % (pos, cols)) or 1
            for pos in ("side", "bottom")
            for cols in (1, 2, 3)
        }

        return self

    def abbrev(self) -> str:
        return self.normal.lower().replace("bottom ", "bot. ").replace("middle ", "mid. ")


class Caption:
    Blank = _Caption("Blank", "")

    Top = _Caption("Top", "Top").transform("Top", "Bottom", "Left").columns(side2=2, side3=3)
    Bottom = _Caption("Bottom", "Bottom").transform("Bottom", "Top", "Right")
    Left = _Caption("Left", "Left").transform("Right", "Left", "Top")
    Right = _Caption("Right", "Right").transform("Left", "Right", "Bottom").columns(bottom2=2, bottom3=3)

    BottomLeft = (
        _Caption("BottomLeft", "Bottom left")
        .transform("BottomRight", "TopLeft", "TopRight")
        .columns(bottom="Bottom", side="Left")
    )
    BottomRight = (
        _Caption("BottomRight", "Bottom right")
        .transform("BottomLeft", "TopRight", "BottomRight")
        .columns(bottom2=2, bottom3=3, bottom="Bottom", side="Right")
    )
    TopLeft = (
        _Caption("TopLeft", "Top left")
        .transform("TopRight", "BottomLeft", "TopLeft")
        .columns(side2=2, side3=3, bottom="Top", side="Left")
    )
    TopRight = (
        _Caption("TopRight", "Top right")
        .transform("TopLeft", "BottomRight", "BottomLeft")
        .columns(side2=2, side3=3, bottom2=2, bottom3=3, bottom="Top", side="Right")
    )

    Center = _Caption("Center", "Center").transform("Center", "Center", "Middle").columns(bottom3=2)
    Middle = _Caption("Middle", "Middle").transform("Middle", "Middle", "Center").columns(side3=2)
    MiddleLeft = (
        _Caption("MiddleLeft", "Middle left")
        .transform("MiddleRight", "MiddleLeft", "CenterTop")
        .columns(side3=2, bottom="Middle", side="Left")
    )
    MiddleRight = (
        _Caption("MiddleRight", "Middle right")
        .transform("MiddleLeft", "MiddleRight", "CenterBottom")
        .columns(side3=2, bottom2=2, bottom3=3, bottom="Middle", side="Right")
    )
    CenterBottom = (
        _Caption("CenterBottom", "Center bottom")
        .transform("CenterBottom", "CenterTop", "MiddleRight")
        .columns(bottom3=2, bottom="Bottom", side="Center")
    )
    CenterTop = (
        _Caption("CenterTop", "Center top")
        .transform("CenterTop", "CenterBottom", "MiddleLeft")
        .columns(bottom3=2, side2=2, side3=3, bottom="Top", side="Center")
    )

    @classmethod
    def flip(cls, flip: str, captions: list[_Caption]) -> list[_Caption] | None:
        if captions:
            return [getattr(cls, getattr(caption, flip)) for caption in captions]
        return None


def get_label(caption_position: str, caption: _Caption, ncols: int) -> str:
    # caption_position = ncols > 1 and caption_position
    if ncols <= 1:
        return caption.normal

    if caption_position == "side":
        return caption.side or caption.normal

    if caption_position in ("bottom", "bottom2"):
        return caption.bottom or caption.normal

    return caption.normal


def get_column(caption_position: str, caption: _Caption, side: str, ncols: int) -> int:
    if caption_position == "side":
        col = caption.caption_column[("side", ncols)]
        if side == "odd":
            return ncols - col + 1

        return col

    return caption.caption_column[("bottom", ncols)]


SPAN = {
    # degenerate
    (Caption.Top, Caption.Bottom): Caption.Blank,
    (Caption.Left, Caption.Right): Caption.Blank,
    # left and right -> top/middle/bottom
    (Caption.TopLeft, Caption.TopRight): Caption.Top,
    (Caption.MiddleLeft, Caption.MiddleRight): Caption.Middle,
    (Caption.BottomLeft, Caption.BottomRight): Caption.Bottom,
    # left, center, right -> top/middle/bottom
    (Caption.TopLeft, Caption.CenterTop, Caption.TopRight): Caption.Top,
    (Caption.MiddleLeft, Caption.Middle, Caption.MiddleRight): Caption.Middle,
    (Caption.BottomLeft, Caption.CenterBottom, Caption.BottomRight): Caption.Bottom,
    # top and bottom -> left/center/right
    (Caption.TopLeft, Caption.BottomLeft): Caption.Left,
    (Caption.CenterTop, Caption.CenterBottom): Caption.Center,
    (Caption.TopRight, Caption.BottomRight): Caption.Right,
    # top, middle, bottom -> left/center/right
    (Caption.TopLeft, Caption.MiddleLeft, Caption.BottomLeft): Caption.Left,
    (Caption.CenterTop, Caption.Middle, Caption.CenterBottom): Caption.Center,
    (Caption.TopRight, Caption.MiddleRight, Caption.BottomRight): Caption.Right,
    # left and center -> left
    (Caption.TopLeft, Caption.CenterTop): Caption.TopLeft,
    (Caption.MiddleLeft, Caption.Middle): Caption.MiddleLeft,
    (Caption.BottomLeft, Caption.CenterBottom): Caption.BottomLeft,
    # center and right -> right
    (Caption.CenterTop, Caption.TopRight): Caption.TopRight,
    (Caption.Middle, Caption.MiddleRight): Caption.MiddleRight,
    (Caption.CenterBottom, Caption.BottomRight): Caption.BottomRight,
    # top and middle -> top
    (Caption.TopLeft, Caption.MiddleLeft): Caption.TopLeft,
    (Caption.CenterTop, Caption.Middle): Caption.CenterTop,
    (Caption.TopRight, Caption.MiddleRight): Caption.TopRight,
    # middle and bottom -> bottom
    (Caption.MiddleLeft, Caption.BottomLeft): Caption.BottomLeft,
    (Caption.Middle, Caption.CenterBottom): Caption.CenterBottom,
    (Caption.MiddleRight, Caption.BottomRight): Caption.BottomRight,
}


def span_captions(*captions: _Caption) -> _Caption:
    if len(captions) == 1:
        return captions[0]

    return SPAN[captions]
