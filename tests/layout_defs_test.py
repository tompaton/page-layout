# pylint: disable=protected-access

from fractions import Fraction

import pytest

from page_layout import layout_defs
from page_layout.layout import Check


def test_generate_layouts() -> None:
    assert len(layout_defs.ALL_LAYOUTS) > 1
    assert layout_defs.ALL_LAYOUTS[0].layout_id == "l1"
    assert "l1" in layout_defs._LAYOUT_DICT


def test_get_layout_by_id() -> None:
    assert layout_defs.get_layout_by_id("l1").layout_id == "l1"


@pytest.mark.parametrize(
    ("orientation", "layout_id"),
    [(None, "l1"), ("landscape", "l1"), ("portrait", "p1"), ("square", "s1")],
)
def test_get_cover_layout(orientation: str, layout_id: str) -> None:
    assert layout_defs.get_cover_layout(orientation) == layout_id


def test_generate_layouts_l1() -> None:
    check = Check()
    gen_slots = layout_defs._generate_layouts_l1()
    assert len(gen_slots) == 1
    assert gen_slots["default"](
        check,
        Fraction(100),
        Fraction(5),
        Fraction("1.25"),
        Fraction("0.8"),
        Fraction(1),
    ) == {
        "check": check,
        "W": Fraction("100"),
        "G": Fraction("5"),
        "L": Fraction("1.25"),
        "P": Fraction("0.8"),
        "S": Fraction("1"),
        "w1": Fraction("100"),
        "h1": Fraction("80.0"),
        "x1": Fraction("0"),
        "y1": Fraction("10.0"),
    }


def test_generate_layouts_grid() -> None:
    check = Check()
    gen_slots = layout_defs._generate_layouts_grid(3, 4, "S")
    assert len(gen_slots) == 1

    assert gen_slots["default"](
        check,
        Fraction(100),
        Fraction(5),
        Fraction("1.25"),
        Fraction("0.8"),
        Fraction(1),
    ) == {
        "h1": Fraction("21.25"),
        "h2": Fraction("21.25"),
        "h3": Fraction("21.25"),
        "h4": Fraction("21.25"),
        "h5": Fraction("21.25"),
        "h6": Fraction("21.25"),
        "h7": Fraction("21.25"),
        "h8": Fraction("21.25"),
        "h9": Fraction("21.25"),
        "h10": Fraction("21.25"),
        "h11": Fraction("21.25"),
        "h12": Fraction("21.25"),
        "w1": Fraction("21.25"),
        "w2": Fraction("21.25"),
        "w3": Fraction("21.25"),
        "w4": Fraction("21.25"),
        "w5": Fraction("21.25"),
        "w6": Fraction("21.25"),
        "w7": Fraction("21.25"),
        "w8": Fraction("21.25"),
        "w9": Fraction("21.25"),
        "w10": Fraction("21.25"),
        "w11": Fraction("21.25"),
        "w12": Fraction("21.25"),
        "x1": Fraction("13.125"),
        "x4": Fraction("13.125"),
        "x7": Fraction("13.125"),
        "x10": Fraction("13.125"),
        "x2": Fraction("39.375"),
        "x5": Fraction("39.375"),
        "x8": Fraction("39.375"),
        "x11": Fraction("39.375"),
        "x3": Fraction("65.625"),
        "x6": Fraction("65.625"),
        "x9": Fraction("65.625"),
        "x12": Fraction("65.625"),
        "y1": Fraction("0.0"),
        "y2": Fraction("0.0"),
        "y3": Fraction("0.0"),
        "y4": Fraction("26.25"),
        "y5": Fraction("26.25"),
        "y6": Fraction("26.25"),
        "y7": Fraction("52.5"),
        "y8": Fraction("52.5"),
        "y9": Fraction("52.5"),
        "y10": Fraction("78.75"),
        "y11": Fraction("78.75"),
        "y12": Fraction("78.75"),
    }
