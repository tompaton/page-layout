from fractions import Fraction
from typing import Any, Optional

from page_layout.aspect import DEFAULT_ASPECT
from page_layout.layout import LayoutParams

ParamKey = tuple[str | None, Fraction | None, str | None]


class Params:
    def __init__(
        self,
        *,
        page_mode: str | None = "default",
        gutter: float | str | Fraction | None = "0.0",
        aspect: str | None = DEFAULT_ASPECT,
        page_colour: str | None = "white",
        caption_position: str = "off",
        full_page: bool = False,
        scale: float | str | Fraction | None = "1.0",
        caption_columns: int = 1,
        caption_font_size: int | None = 8,
        shift_page_number: bool | None = True,
    ) -> None:
        self.page_mode = page_mode
        self.gutter: Fraction | None = None if gutter is None else Fraction(gutter)
        self.aspect = aspect
        self.page_colour = page_colour
        self.caption_position = caption_position
        self.full_page = full_page
        self.scale: Fraction | None = None if scale is None else Fraction(scale)
        self.caption_columns = caption_columns
        self.caption_font_size = caption_font_size
        self.shift_page_number = shift_page_number

    def key(self) -> ParamKey:
        return (self.page_mode, self.gutter, self.aspect)

    def layout_params(self, page_width: Fraction) -> LayoutParams:
        return LayoutParams(
            page_width,
            self.page_mode,
            self.gutter,
            self.aspect,
            self.caption_position,
            self.caption_columns,
        )

    def options(self, default: Optional["Params"] = None) -> "Params":
        if default is None:
            return self

        d = {
            "full_page": self.full_page,
            "scale": self.scale,
            "caption_columns": self.caption_columns,
        }
        for k in [
            "aspect",
            "caption_position",
            "gutter",
            "page_colour",
            "page_mode",
            "caption_font_size",
            "shift_page_number",
        ]:
            v = getattr(self, k)
            if v is None:
                d[k] = getattr(default, k)
            else:
                d[k] = v
        return type(self)(**d)  # type: ignore

    @classmethod
    def from_book_dict(cls, d: dict[str, Any]) -> "Params":
        # use defaults for omitted values
        return cls(
            page_mode=d.get("page_mode", "default"),
            gutter=d.get("gutter", "0.0"),
            aspect=d.get("aspect", DEFAULT_ASPECT),
            page_colour=d.get("page_colour", "white"),
            caption_position=d.get("caption_position", "off"),
            full_page=d.get("full_page", False),
            scale=d.get("scale", "1.0"),
            caption_columns=d.get("caption_columns", 1),
            caption_font_size=d.get("caption_font_size", 8),
            shift_page_number=d.get("shift_page_number", True),
        )

    @classmethod
    def from_page_dict(cls, d: dict[str, Any]) -> "Params":
        # use None for omitted values so they will fall back to book
        # defaults in options method
        return cls(
            page_mode=d.get("page_mode"),
            gutter=d.get("gutter"),
            aspect=d.get("aspect"),
            page_colour=d.get("page_colour"),
            caption_position=d.get("caption_position", "off"),
            full_page=d.get("full_page", False),
            scale=d.get("scale", "1.0"),
            caption_columns=d.get("caption_columns", 1),
            caption_font_size=d.get("caption_font_size"),
            shift_page_number=d.get("shift_page_number"),
        )

    @classmethod
    def for_cover(cls, mode: str) -> "Params":
        return cls(
            page_mode=None,
            gutter=None,
            aspect=None,
            page_colour=None,
            caption_position="bottom",
            full_page=mode == "front",
            scale=Fraction("0.75") if mode == "back" else Fraction(1),
        )
