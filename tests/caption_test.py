from typing import Any

import pytest

from page_layout.caption import Caption, _Caption, get_column, get_label


def test_abbrev() -> None:
    abbrevs = {
        getattr(Caption, attr).normal: getattr(Caption, attr).abbrev()
        for attr in dir(Caption)
        if isinstance(getattr(Caption, attr), _Caption)
    }

    assert abbrevs == {
        "": "",
        "Top": "top",
        "Bottom": "bottom",
        "Left": "left",
        "Right": "right",
        "Bottom left": "bot. left",
        "Bottom right": "bot. right",
        "Top left": "top left",
        "Top right": "top right",
        "Center": "center",
        "Middle": "middle",
        "Middle left": "mid. left",
        "Middle right": "mid. right",
        "Center bottom": "center bottom",
        "Center top": "center top",
    }


# the to_dict method has been removed, but keeping the tests
def to_dict(caption: _Caption) -> dict[str, Any]:
    return {
        "label": caption.normal,
        "side_label": caption.side or caption.normal,
        "bottom_label": caption.bottom or caption.normal,
        "columns": {pos: [caption.caption_column[(pos, cols)] for cols in (1, 2, 3)] for pos in ("side", "bottom")},
    }


def test_to_dict() -> None:
    captions = [getattr(Caption, attr) for attr in dir(Caption) if isinstance(getattr(Caption, attr), _Caption)]

    assert {caption.caption_id: to_dict(caption) for caption in captions} == {
        "Blank": {
            "label": "",
            "side_label": "",
            "bottom_label": "",
            "columns": {"bottom": [1, 1, 1], "side": [1, 1, 1]},
        },
        "Top": {
            "label": "Top",
            "side_label": "Top",
            "bottom_label": "Top",
            "columns": {"bottom": [1, 1, 1], "side": [1, 2, 3]},
        },
        "Bottom": {
            "label": "Bottom",
            "side_label": "Bottom",
            "bottom_label": "Bottom",
            "columns": {"bottom": [1, 1, 1], "side": [1, 1, 1]},
        },
        "Left": {
            "label": "Left",
            "side_label": "Left",
            "bottom_label": "Left",
            "columns": {"bottom": [1, 1, 1], "side": [1, 1, 1]},
        },
        "Right": {
            "label": "Right",
            "side_label": "Right",
            "bottom_label": "Right",
            "columns": {"bottom": [1, 2, 3], "side": [1, 1, 1]},
        },
        "BottomLeft": {
            "label": "Bottom left",
            "side_label": "Left",
            "bottom_label": "Bottom",
            "columns": {"bottom": [1, 1, 1], "side": [1, 1, 1]},
        },
        "BottomRight": {
            "label": "Bottom right",
            "side_label": "Right",
            "bottom_label": "Bottom",
            "columns": {"bottom": [1, 2, 3], "side": [1, 1, 1]},
        },
        "TopLeft": {
            "label": "Top left",
            "side_label": "Left",
            "bottom_label": "Top",
            "columns": {"bottom": [1, 1, 1], "side": [1, 2, 3]},
        },
        "TopRight": {
            "label": "Top right",
            "side_label": "Right",
            "bottom_label": "Top",
            "columns": {"bottom": [1, 2, 3], "side": [1, 2, 3]},
        },
        "Center": {
            "label": "Center",
            "side_label": "Center",
            "bottom_label": "Center",
            "columns": {"bottom": [1, 1, 2], "side": [1, 1, 1]},
        },
        "Middle": {
            "label": "Middle",
            "side_label": "Middle",
            "bottom_label": "Middle",
            "columns": {"bottom": [1, 1, 1], "side": [1, 1, 2]},
        },
        "MiddleLeft": {
            "label": "Middle left",
            "side_label": "Left",
            "bottom_label": "Middle",
            "columns": {"bottom": [1, 1, 1], "side": [1, 1, 2]},
        },
        "MiddleRight": {
            "label": "Middle right",
            "side_label": "Right",
            "bottom_label": "Middle",
            "columns": {"bottom": [1, 2, 3], "side": [1, 1, 2]},
        },
        "CenterBottom": {
            "label": "Center bottom",
            "side_label": "Center",
            "bottom_label": "Bottom",
            "columns": {"bottom": [1, 1, 2], "side": [1, 1, 1]},
        },
        "CenterTop": {
            "label": "Center top",
            "side_label": "Center",
            "bottom_label": "Top",
            "columns": {"bottom": [1, 1, 2], "side": [1, 2, 3]},
        },
    }


@pytest.mark.parametrize(
    ("cols", "pos", "columns"),
    [
        (
            1,
            "bottom",
            [
                [
                    "",
                    "Bottom",
                    "Bottom left",
                    "Bottom right",
                    "Center",
                    "Center bottom",
                    "Center top",
                    "Left",
                    "Middle",
                    "Middle left",
                    "Middle right",
                    "Right",
                    "Top",
                    "Top left",
                    "Top right",
                ]
            ],
        ),
        (
            1,
            "side",
            [
                [
                    "",
                    "Bottom",
                    "Bottom left",
                    "Bottom right",
                    "Center",
                    "Center bottom",
                    "Center top",
                    "Left",
                    "Middle",
                    "Middle left",
                    "Middle right",
                    "Right",
                    "Top",
                    "Top left",
                    "Top right",
                ]
            ],
        ),
        (
            2,
            "bottom",
            [
                [
                    "",
                    "Bottom",
                    "Bottom left",
                    "Center",
                    "Center bottom",
                    "Center top",
                    "Left",
                    "Middle",
                    "Middle left",
                    "Top",
                    "Top left",
                ],
                ["Bottom right", "Middle right", "Right", "Top right"],
            ],
        ),
        (
            2,
            "side",
            [
                [
                    "",
                    "Bottom",
                    "Bottom left",
                    "Bottom right",
                    "Center",
                    "Center bottom",
                    "Left",
                    "Middle",
                    "Middle left",
                    "Middle right",
                    "Right",
                ],
                ["Center top", "Top", "Top left", "Top right"],
            ],
        ),
        (
            3,
            "bottom",
            [
                [
                    "",
                    "Bottom",
                    "Bottom left",
                    "Left",
                    "Middle",
                    "Middle left",
                    "Top",
                    "Top left",
                ],
                ["Center", "Center bottom", "Center top"],
                ["Bottom right", "Middle right", "Right", "Top right"],
            ],
        ),
        (
            3,
            "side",
            [
                [
                    "",
                    "Bottom",
                    "Bottom left",
                    "Bottom right",
                    "Center",
                    "Center bottom",
                    "Left",
                    "Right",
                ],
                ["Middle", "Middle left", "Middle right"],
                ["Center top", "Top", "Top left", "Top right"],
            ],
        ),
    ],
)
def test_columns(cols: int, pos: str, columns: list[list[str]]) -> None:
    captions = [getattr(Caption, attr) for attr in dir(Caption) if isinstance(getattr(Caption, attr), _Caption)]

    assert [
        sorted([caption.normal for caption in captions if caption.caption_column[(pos, cols)] == col])
        for col in range(1, cols + 1)
    ] == columns


def test_flip_none() -> None:
    assert Caption.flip("flip_h", []) is None


@pytest.mark.parametrize(
    ("flip", "result"),
    [
        (
            "flip_h",
            {
                "": "",
                "Top": "Top",
                "Bottom": "Bottom",
                "Left": "Right",
                "Right": "Left",
                "Bottom left": "Bottom right",
                "Bottom right": "Bottom left",
                "Top left": "Top right",
                "Top right": "Top left",
                "Center": "Center",
                "Middle": "Middle",
                "Middle left": "Middle right",
                "Middle right": "Middle left",
                "Center bottom": "Center bottom",
                "Center top": "Center top",
            },
        ),
        (
            "flip_v",
            {
                "": "",
                "Top": "Bottom",
                "Bottom": "Top",
                "Left": "Left",
                "Right": "Right",
                "Bottom left": "Top left",
                "Bottom right": "Top right",
                "Top left": "Bottom left",
                "Top right": "Bottom right",
                "Center": "Center",
                "Middle": "Middle",
                "Middle left": "Middle left",
                "Middle right": "Middle right",
                "Center bottom": "Center top",
                "Center top": "Center bottom",
            },
        ),
        (
            "transpose",
            {
                "": "",
                "Top": "Left",
                "Bottom": "Right",
                "Left": "Top",
                "Right": "Bottom",
                "Bottom left": "Top right",
                "Bottom right": "Bottom right",
                "Top left": "Top left",
                "Top right": "Bottom left",
                "Center": "Middle",
                "Middle": "Center",
                "Middle left": "Center top",
                "Middle right": "Center bottom",
                "Center bottom": "Middle right",
                "Center top": "Middle left",
            },
        ),
    ],
)
def test_flip(flip: str, result: dict[str, str]) -> None:
    captions = [getattr(Caption, attr) for attr in dir(Caption) if isinstance(getattr(Caption, attr), _Caption)]
    flipped = Caption.flip(flip, captions)
    assert flipped is not None
    assert {
        caption.normal: flipped_caption.normal for caption, flipped_caption in zip(captions, flipped, strict=False)
    } == result


@pytest.mark.parametrize(
    ("caption_position", "caption", "ncols", "label"),
    [
        ("off", Caption.CenterBottom, 1, "Center bottom"),
        ("off", Caption.CenterBottom, 2, "Center bottom"),
        ("side", Caption.CenterBottom, 2, "Center"),
        ("bottom", Caption.CenterBottom, 2, "Bottom"),
        ("bottom2", Caption.CenterBottom, 2, "Bottom"),
        ("next", Caption.CenterBottom, 2, "Center bottom"),
    ],
)
def test_get_label(caption_position: str, caption: _Caption, ncols: int, label: str) -> None:
    assert get_label(caption_position, caption, ncols) == label


@pytest.mark.parametrize(
    ("caption_position", "caption", "side", "ncols", "column"),
    [
        ("side", Caption.CenterBottom, "odd", 1, 1),
        ("side", Caption.CenterBottom, "odd", 2, 2),
        ("side", Caption.CenterBottom, "odd", 3, 3),
        ("side", Caption.CenterBottom, "even", 1, 1),
        ("side", Caption.CenterBottom, "even", 2, 1),
        ("side", Caption.CenterBottom, "even", 3, 1),
        ("bottom", Caption.CenterBottom, "even", 1, 1),
        ("bottom", Caption.CenterBottom, "even", 2, 1),
        ("bottom", Caption.CenterBottom, "even", 3, 2),
        ("bottom", Caption.CenterBottom, "odd", 1, 1),
        ("bottom", Caption.CenterBottom, "odd", 2, 1),
        ("bottom", Caption.CenterBottom, "odd", 3, 2),
        ("bottom2", Caption.CenterBottom, "even", 1, 1),
        ("bottom2", Caption.CenterBottom, "even", 2, 1),
        ("bottom2", Caption.CenterBottom, "even", 3, 2),
    ],
)
def test_get_column(caption_position: str, caption: _Caption, side: str, ncols: int, column: int) -> None:
    assert get_column(caption_position, caption, side, ncols) == column
