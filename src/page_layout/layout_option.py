from collections.abc import Iterator
from typing import Any

from page_layout.layout import Layout

Photo = Any

AltLayout = tuple[int, "LayoutOption"]

ColumnType = list[tuple[bool, str, str]]


class LayoutOption:
    def __init__(self, remaining_photos: int, layout: Layout) -> None:
        self.layout = layout
        self.remaining_photos = remaining_photos

    def __repr__(self) -> str:
        return f"<LayoutOption layout_id={self.layout.layout_id}>"

    @property
    def num_slots(self) -> int:
        return self.layout.num_slots


def get_options(enabled_layouts: list[Layout], orientations: list[str]) -> list[list[LayoutOption]]:
    # create list of usable layouts for each number of photos remaining.
    # layout chain is created by selecting layout from a column, 'using' the
    # photos and 'jumping' to the column for the now remaining number of
    # photos.

    options: list[list[LayoutOption]] = [[]]  # 0 photos remaining -- no options
    for remaining_photos in range(1, len(orientations) + 1):
        options.append([])
        for layout in enabled_layouts:
            if layout.is_valid(len(orientations[-remaining_photos:]), orientations[-remaining_photos:]):
                options[-1].append(LayoutOption(remaining_photos, layout))

    return options


def join_caption(columns: list[ColumnType]) -> Iterator[list[dict[str, str]]]:
    for column in columns:
        # don't show label if there is only one photo in column
        show_label = len(column) != 1

        yield [
            {"label": label if show_label and label else "", "caption": photo_caption}
            for show_caption, label, photo_caption in column
            if show_caption and photo_caption
        ]
