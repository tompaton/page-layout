# Page Layout

Layout for photo book pages.


[![PyPI - Version](https://img.shields.io/pypi/v/page-layout.svg)](https://pypi.org/project/page-layout)
[![PyPI - Python Version](https://img.shields.io/pypi/pyversions/page-layout.svg)](https://pypi.org/project/page-layout)

-----

## Table of Contents

- [Installation](#installation)
- [License](#license)

## Installation

```console
pip install page-layout
```

## Layout options ###

![Layout option thumbnails](docs/layouts.png)

## License

`page-layout` is distributed under the terms of the [MIT](https://spdx.org/licenses/MIT.html) license.
